/// <reference types="cypress"/>

class AccountPage {
  visit() {
    cy.visit('/account/join')
  }

  enterEmailAddress(data) {
    this.enterData('#email', data)
  }

  enterPassword(data) {
    this.enterData('#password', data)
  }

  enterFirstName(data) {
    this.enterData('#first-name', data)
  }

  enterLastName(data) {
    this.enterData('#last-name', data)
  }

  enterMobileNumber(data) {
    this.enterData('#mobile-phone', data)
  }

  enterDOB(data) {
    this.enterData('#date-of-birth', data)
  }

  enterAddress(data) {
    cy.get('#address').type(data)
  }

  enterAddressLine1(data) {
    cy.get('#addressLine1').type(data)
  }

  enterAddressFinder(data) {
    this.enterAddress(data)
    this.clickElement(
      'ul[data-automation="address-suggestion"]>li:nth-of-type(1)',
    )
  }

  clickEnterAddressManually() {
    this.clickElement(
      '#root > div.css-xdocc1 > div.css-1jbjeho > form > div.css-tgzwmh > div > button',
    )
  }

  enterSuburb(data) {
    cy.get('#city').type(data)
  }

  enterPostCode(data) {
    cy.get('#postcode').type(data)
  }

  enterState(data) {
    cy.get('#stateCode').click().get('select').select(data, { force: true })
  }

  clickFindCardNumber() {
    this.clickElement('[data-automation="single-signup-myerone-lookup-button"]')
  }

  clickJoinButton() {
    this.clickElement('#join')
  }

  clickCreateAccountButton() {
    this.clickElement('#create-account')
  }

  assertAccountHeading(data) {
    this.assertText('#accountHeading', data)
  }

  assertEmailError(data) {
    this.assertText('#email-error-text', data)
  }

  assertPasswordError(data) {
    this.assertText('#password-error-text', data)
  }

  assertFirstNameError(data) {
    this.assertText('#first-name-error-text', data)
  }

  assertLastNameError(data) {
    this.assertText('#last-name-error-text', data)
  }

  assertMobileNumberError(data) {
    this.assertText('#mobile-phone-error-text', data)
  }

  assertDOBError(data) {
    this.assertText('#date-of-birth-error-text', data)
  }

  assertMyeroneError(data) {
    this.assertText('#single-signup-myerone-error', data)
  }

  assertAddressFinderError(data) {
    this.assertText('#address-error-text', data)
  }

  assertMyeroneNumberPresent() {
    cy.get('#myer-one-field').invoke('val').should('have.lengthOf', 6)
  }

  assertAccountMenu() {
    cy.get('[data-automation="header-account"]').should(
      'have.length.greaterThan',
      0,
    )
  }

  assertMyerOnlineTerms(data) {
    this.assertHrefAttribute('[data-automation="join-terms-link"]', data)
  }

  assertMyerOneTerms(data) {
    this.assertHrefAttribute(
      '[data-automation="myer-one-join-terms-link"]',
      data,
    )
  }

  assertMyerPrivacy(data) {
    this.assertHrefAttribute('[data-automation="join-privacy-link"]', data)
  }

  enterData(selector, data) {
    cy.get(selector).type(data)
  }

  clickElement(selector) {
    cy.get(selector).click()
  }

  assertText(selector, text) {
    cy.get(selector).should('contain.text', text)
  }

  assertHrefAttribute(selector, data) {
    cy.get(selector).invoke('attr', 'href').should('equal', data)
  }
}

export default new AccountPage()
