Feature: Create new MYER account(s)
  As a customer, 
  I want to be able to register MYER account,
  So I can order the products from the website

Background:
  Given I go to "Create Account" page

@smoke
Scenario: Create MYER online account and MYER one
  When I enter email address "juliio.tran@gmail.com"
  And I click "Join" button
  And I enter password "P@ssw0rd123"
  And I enter firstname "Juliio"
  And I enter lastname "QAE"
  And I enter mobilenumber "0459433400"
  And I enter DOB "01/01/2000"
  And I enter address finder "8 O'Brien Parade, LIVERPOOL  NSW 2170"
  And I click "Create Account" button
  Then I see my firstname "Juliio" at account header
  And I see the "Account" menu at top right

@smoke
Scenario: Create MYER online account and search for MYER one
  When I click "Create Account" button
  And I enter email address "juliio.tran@gmail.com"
  And I enter password "P@ssw0rd123"
  And I enter firstname "Juliio"
  And I enter lastname "QAE"
  And I enter mobilenumber "0459433400"
  And I enter address finder "8 O'Brien Parade, LIVERPOOL  NSW 2170"
  And I click "Find Card Number" button
  Then I see the myerone error message "A MYER one membership number wasn\'t found. If you have a membership number please enter it below."
  And I see my myerone number is reveal on screen
  When I click "Create Account" button
  Then I see my firstname "Juliio" at account header
  And I see the "Account" menu at top right

@smoke
Scenario: Legal links when first opening Create New Account form
  Then I see correct Myer online "Terms and Conditions" URL
  And I see correct Myer one "Terms and Conditions" URL
  And I see correct "Privacy Policy" URL

@smoke
Scenario: Legal links after entering email adddress
  When I enter email address "juliio.tran@gmail.com"
  And I click "Join" button
  Then I see correct Myer online "Terms and Conditions" URL
  And I see correct Myer one "Terms and Conditions" URL
  And I see correct "Privacy Policy" URL

@smoke
Scenario: An error message when entering invalid email address
  When I enter email address "juliio.tran@@gmail.com"
  And I click "Join" button
  Then I see email error message "Please enter a valid email address"

@smoke
Scenario: Validation for mandatory fields
  When I enter email address "juliio.tran@gmail.com"
  And I click "Join" button
  And I click "Create Account" button
  Then I see password error message "Please enter a valid password"
  And I see firstname error message "Please enter a valid name"
  And I see lastname error message "Please enter a valid name"
  And I see mobilenumber error message "Please enter a valid Australian mobile phone number"
  And I see dob error message "Please enter a valid birthday"
  And I see address error message "Please enter a valid address"

@smoke
Scenario: Validation for mandatory fields for already MYER one member
  When I click "Create Account" button
  And I click "Create Account" button 
  And I see email error message "Please enter a valid email address"
  And I see password error message "Please enter a valid password"
  And I see firstname error message "Please enter a valid name"
  And I see lastname error message "Please enter a valid name"
  And I see mobilenumber error message "Please enter a valid Australian mobile phone number"
  And I see address error message "Please enter a valid address"

@smoke
Scenario: Validation eligibility age for MYER one Member
  When I enter email address "juliio.tran@gmail.com"
  And I click "Join" button
  And I enter DOB which is below 15 years old
  And I click "Create Account" button
  Then I see dob error message "Please note, you must be 15 years or older to be a MYER one Member."

@smoke
Scenario: Create online account and Join MYER one with Manual Add Address
  When I enter email address "juliio.tran@gmail.com"
  And I click "Join" button
  And I enter password "P@ssw0rd123"
  And I enter firstname "Juliio"
  And I enter lastname "QAE"
  And I enter mobilenumber "0459433400"
  And I enter DOB "01/01/2000"
  And I enter address "8 Orbelllllllllll"
  When I click manual address "Enter Address Manually" button
  And I enter address1 "8 O'Brien Parade"
  And I enter suburb "LIVERPOOL"
  And I enter postcode "2170"
  And I enter state "NSW"
  And I click "Create Account" button
  Then I see my firstname "Juliio" at account header
  And I see the "Account" menu at top right