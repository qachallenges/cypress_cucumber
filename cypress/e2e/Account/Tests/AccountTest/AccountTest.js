import { Given, When, Then, And } from 'cypress-cucumber-preprocessor/steps'
import AccountPage from '../../Pages/AccountPage/AccountPage'
import Utils from '../Utils/Utils'

Given('I go to "Create Account" page', () => {
  AccountPage.visit()
})

When('I enter email address {string}', email => {
  AccountPage.enterEmailAddress(Utils.RandomNumber() + email)
})

And('I click "Join" button', () => {
  AccountPage.clickJoinButton()
})

And('I enter password {string}', password => {
  AccountPage.enterPassword(password)
})

And('I enter firstname {string}', firstname => {
  AccountPage.enterFirstName(firstname)
})

And('I enter lastname {string}', lastname =>
  AccountPage.enterLastName(lastname),
)

And('I enter mobilenumber {string}', mobilenumber =>
  AccountPage.enterMobileNumber(mobilenumber),
)

And('I enter DOB {string}', dob => AccountPage.enterDOB(dob))

And('I enter address finder {string}', address => {
  AccountPage.enterAddressFinder(address)
})
And('I click "Create Account" button', () =>
  AccountPage.clickCreateAccountButton(),
)

Then('I see my firstname {string} at account header', firstname =>
  AccountPage.assertAccountHeading(firstname),
)

Then('I see the error message {string}', message =>
  AccountPage.assertEmailError(message),
)

And('I click "Find Card Number" button', () =>
  AccountPage.clickFindCardNumber(),
)

Then('I see the myerone error message {string}', message =>
  AccountPage.assertMyeroneError(message),
)

And('I see my myerone number is reveal on screen', () =>
  AccountPage.assertMyeroneNumberPresent(),
)

And('I see the "Account" menu at top right', () =>
  AccountPage.assertAccountMenu(),
)
Then('I see correct Myer online "Terms and Conditions" URL', () =>
  AccountPage.assertMyerOnlineTerms('/content/terms-conditions'),
)
Then('I see correct Myer one "Terms and Conditions" URL', () =>
  AccountPage.assertMyerOneTerms('/content/myer-one-terms-conditions'),
)
Then('I see correct "Privacy Policy" URL', () =>
  AccountPage.assertMyerPrivacy('/content/privacy'),
)
Then('I see password error message {string}', message =>
  AccountPage.assertPasswordError(message),
)
Then('I see firstname error message {string}', message =>
  AccountPage.assertFirstNameError(message),
)
Then('I see lastname error message {string}', message =>
  AccountPage.assertLastNameError(message),
)
Then('I see mobilenumber error message {string}', message =>
  AccountPage.assertMobileNumberError(message),
)
Then('I see dob error message {string}', message =>
  AccountPage.assertDOBError(message),
)
Then('I see address error message {string}', message =>
  AccountPage.assertAddressFinderError(message),
)
And('I click manual address "Enter Address Manually" button', () =>
  AccountPage.clickEnterAddressManually(),
)

Then('I see email error message {string}', message =>
  AccountPage.assertEmailError(message),
)

And('I enter address1 {string}', address =>
  AccountPage.enterAddressLine1(address),
)
And('I enter suburb {string}', city => AccountPage.enterSuburb(city))
And('I enter postcode {string}', postcode =>
  AccountPage.enterPostCode(postcode),
)
And('I enter state {string}', state => AccountPage.enterState(state))
And('I enter address {string}', address => {
  AccountPage.enterAddress(address)
})

When('I enter DOB which is below {int} years old', years =>
  AccountPage.enterDOB(Utils.subtractYears(years)),
)
