class Utils {
  static RandomNumber() {
    return Math.floor(Math.random() * 1000000000)
  }

  static subtractYears(years) {
    const date = new Date()
    date.setFullYear(
      date.getFullYear() - years,
      date.getMonth(),
      date.getDate() + 1,
    )

    return date.toLocaleDateString('en-AU')
  }
}

export default Utils
