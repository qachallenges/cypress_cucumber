# cypress_cucumber

## Setup

- Clone the repository
- From Terminal: run command `npm install`

## Open Cypress

- From Terminal: run command `npm run cy:open`
- From Cypress Dashboard: click E2E Testing choose "Chrome", then click "Start
  E2E Testing in Chrome" click "Account.feature"

## Run Cypress

- From Terminal: run command `npx cypress run --env tags="@smoke"`
